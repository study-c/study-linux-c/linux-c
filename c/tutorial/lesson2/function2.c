#include <stdio.h>
#include <stdlib.h>

int add1(int i);

int add2(int *i);

int main(int argc,char *argv[]){
    int a = 3,b =3;
    printf("Before calling add functions a=%d, b=%d \n",a,b);
    int c = add1(a);
    int d = add2(&b);
    printf("After calling add functions a=%d, b=%d \n",a,b);
    printf("The return values a=%d, b=%d \n",a,b);
    return 0;
}

int add1(int i){
    i = i + 1;
    printf("during add1 parameter i = %d \n",i);
    return i;
}

int add2(int* i){
    *i = *i +2;
    printf("during add2 parameter i = %d \n",*i);
    return *i;
}
