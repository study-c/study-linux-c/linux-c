#include <stdio.h>
#include <stdlib.h>
typedef struct game_state gameSate;
struct game_state{
    int point;
    int wealth;
};
void setStructValue(gameSate *s){
    s->point = 11;
    s->wealth = 22;
}
int main(int argc, char *argv[]){
    gameSate s = {1,2};   
    struct game_state b = {1,2};
    gameSate *ptr;
    ptr = (gameSate *)malloc(sizeof(gameSate));
    ptr->point = 4;
    ptr->wealth = 6;
    printf("s.point:%d s.wealth:%d \n",s.point,s.wealth);
    printf("b.point:%d b.wealth:%d \n",b.point,b.wealth);
    printf("ptr point:%d ptr wealth:%d \n",ptr->point,ptr->wealth);
    printf("ptr point:%d ptr wealth:%d \n",(*ptr).point,(*ptr).wealth);
   
    setStructValue(&s);
    printf("s.point:%d s.wealth:%d \n",s.point,s.wealth);
    setStructValue(ptr);
    printf("ptr point:%d ptr wealth:%d \n",ptr->point,ptr->wealth);

    free(ptr);

    return 0;
}

