#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    int a = 3;
    int b = a;

    printf("a:%d b:%d \n",a,b);
    printf("address of a:%p \n", &a);
    a = 6;

    printf("a:%d b:%d \n",a,b);

    int * c;
    c = &a;
    printf("size of point c: %d \n",sizeof(c));
    int * d;
    d = c;
    
    printf("*c:%d *d:%d a:%d \n",*c,*d,a);

    a = 5;

    printf("*c:%d *d:%d a:%d \n",*c,*d,a);
}
