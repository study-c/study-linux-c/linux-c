#ifndef NODE_H_
#define NODE_H_

typedef struct node{
    int data;
    struct node *next;
} Node;
void init(Node *n);

Node * initWithValue( int a);

#endif
