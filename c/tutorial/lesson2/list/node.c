#include <stdlib.h>
#include "node.h"

Node * initWithValue(int a){
    Node *n = (Node *)malloc(sizeof(Node)); 
    n->data = a;
    n->next = NULL;
    return n;
}
