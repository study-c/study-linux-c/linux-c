#include <stdio.h>
#include <stdlib.h>
#include "node.h"

int main(int argc, char *agrv[])
{
	node t,t1,t2;
  	int a = 3;
    char *s = "hello";
  	init_node(&t,&a);
  	printf("%d \n",*(int *)getDataN(t));
    setDataN(t1,s);
    printf("%s \n",getDataN(t1));
    setNextN(t,t1);
    printf("t1 address:%p\nt->next address %p\n",t1,getNextN(t));
  	return 0;

}
