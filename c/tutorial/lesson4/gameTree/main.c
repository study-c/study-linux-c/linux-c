#include <stdio.h>
#include <stdlib.h>
#include "node.h"
#include "stack.h"
#include "gameState.h"
#include "tNode.h"
#include "gameTree.h"

int main(int argc, char *agrv[])
{
	node t,t1,t2;
  	int a = 5;
    	char *s = "hello";
  	char *s1 = "good";
  	char *s2 = "day";
  	init_node(&t,&a);
  	init_node(&t1,s);
	printf("*********************test of node  ****************************\n");
  	printf("%d \n",*(int *)getDataN(t)); //5
    	setDataN(t1,s1);
    	printf("%s \n",getDataN(t1)); // good
    	setNextN(t,t1);
    	printf("Node t1 address:%p\nt->next address %p\n",t1,getNextN(t));//address of node t1 and t2
	printf("*********************test of stack ****************************\n");

    	stack tests;
	init_stack(&tests);

  	printf("Stack: tests address:%p\n",tests);// address of stack

  	printf("After initialize test IsEmpty:%s \n", isEmptyS(tests) ? "True" :"False"); //true

  	push(tests,&a);

	printf("After push a test IsEmpty:%s \n", isEmptyS(tests) ? "True" :"False");//false
   	printf("*********************push method test ****************************\n");
	push(tests,t1);

  	push(tests,s);
  	push(tests,s1);

  	printf("%s \n",toStringS(tests,"a"));// a a a a
  	printf("*********************top method test ****************************\n");
  	printf("%s \n",(char *)top(tests)); // good
  	printf("%s \n",toStringS(tests,"a")); // a a a a
    	printf("*********************pop method test ****************************\n");
	pop(tests);
  	printf("%s \n",(char *)top(tests)); //hello
  	pop(tests);
	pop(tests);
	pop(tests);
	printf("%s \n",toStringS(tests,"a"));// <>
      	printf("*********************test gameSate ****************************\n");
	gameState g,g2;
  	int gameStart = 21, gameThree = 3;
  	init_gameState(&g,gameStart,0);
  	init_gameState(&g2,a,4);
  	printf("g: points: %d \n",getCount(g)); //21
	printf("g: worth: %d \n",getWorth(g)); //0
  	setCount(g,gameThree);
  	setWorth(g,evaluateState(g,false));
  	printf("g: points: %d \n",getCount(g));// 3
	printf("g: worth: %d \n",getWorth(g));// 3
    	setCount(g,gameStart);
  	setWorth(g,evaluateState(g,false));
  	printf("*********************tNode test ****************************\n");
	tNode tn,tn1;
	init_TNode(&tn,g,0);
  	gameState g1 = (gameState)getTNData(tn);
  	int le1 = getTNLevel(tn);
  	init_TNode(&tn1,g1,9);
  	printf("level of tNode: %d \n",le1);//0
	printf("gameState in tNode : \n point: %d \n worth : %d \n",getCount(g1),getWorth(g1));

	setTNData(tn,s);
  	setTNLevel(tn,22);
    	printf("level of tNode: %d \n",le1);
	printf("String in tNode :  %s \n",getTNData(tn));

  	setTNChild(tn,tn1);

     	printf("level of tn1 : %d \n",getTNLevel(getTNChild(tn)));
	printf("String in tn1 :  %d \n",getCount(getTNData(getTNChild(tn))));
    	printf("*********************gameTree test ****************************\n");
                                                        	gameTree root,leaf1,leaf2;
	init_gameTree(&root,true,g,0);
  	printf("empty game Tree :  %s \n",toStringGT(root));

 	init_gameTree(&root,false,g,0);
  	printf("Leaf Tree :  %s \n",toStringGT(root));

	init_gameTree(&leaf1,false,g1,5);
  	init_gameTree(&leaf2,false,g2,5);
  	setChild(root,leaf1);
  	setSibling(root,leaf2);
  	printf("game Tree :  %s \n",toStringGT(root));
  	return 0;

}
