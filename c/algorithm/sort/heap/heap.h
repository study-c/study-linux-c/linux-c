#ifndef HEAP_H
#define HEAP_H
#define MAX_HEAP 1000
struct heap_str;
typedef struct heap_str *heap;

heap heap_int(int size);
void destroy_heap(heap h);
void swap(heap h, int a, int b);
void max_heapify(heap h, int position);
heap build_heap(int *arr, int array_size);
void print_heap(heap h);
void heap_insert(heap h, int value);

#endif
