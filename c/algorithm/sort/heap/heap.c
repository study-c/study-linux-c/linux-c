#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "heap.h"

struct heap_str {
  int *arr;
  size_t arr_size;
  size_t heap_size;
};

heap heap_int(int size) {
  heap h;
  h = (heap) malloc(sizeof(struct heap_str));
  h->arr = malloc(sizeof(int)*(MAX_HEAP));
  h->arr_size = size;
  h->heap_size = 0;
  return h;
};

void destroy_heap(heap h) {
  free(h->arr);
  free(h);
};

void swap(heap h, int a, int b) {
  int tem;
  tem = h->arr[a];
  h->arr[a] = h->arr[b];
  h->arr[b] = tem;
};

void max_heapify(heap h, int position) {
  int left, right, largest;
  largest = position;
  left = position * 2 + 1;
  right = left + 1;

  if(left < h->heap_size && h->arr[left] > h->arr[largest]) {
    largest = left;
  }

  if(right < h->heap_size && h->arr[right] > h->arr[largest]) {
    largest = right;
  }

  if(largest != position) {
    swap(h, position, largest);
    max_heapify(h, largest);
  }
};

heap build_heap(int *arr, int array_size) {
  heap h = heap_int(array_size);
  memcpy((h->arr), arr, sizeof(int) * array_size);
  h->arr_size = array_size;
  h->heap_size = array_size;

  for(int i = (array_size - 1) / 2; i >= 0; i--) {
    max_heapify(h, i);
  }

  for(int i = array_size -1; i >= 0; i--) {
    swap(h, 0, i);
    h->heap_size--;
    max_heapify(h, 0);
  }

  return h;
}

void print_heap(heap h) {
  for (int i = 0; i < h->arr_size; i++) {
    printf("%d\n", h->arr[i]);
  }
};

void heap_insert(heap h, int value) {
  
}
