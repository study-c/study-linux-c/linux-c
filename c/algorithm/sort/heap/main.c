#include <stdio.h>
#include "heap.h"

int main(int argc, char **argv) {
  int size = 15;
  int arr[15] = { 4, 3, 5, 1, 4, 10, 123, 3, 47, 11, 432, 1, 54, 85, 99};
  heap h = build_heap(arr, size);
  print_heap(h);
  return 0;
}
