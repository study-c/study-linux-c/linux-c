#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

int main(int agrc, char **agrv) {
    int n;
    int fd[2];
    pid_t pid;
    char line[20];
    if (pipe(fd) < 0) {
         perror("Error create pipe");
         exit(1);
    }
    pid = fork();
    if (pid < 0) {
      perror("Error fork");
      exit(1);
    } else if (pid == 0) { //child
      close(fd[1]);
      n = read(fd[0], line, 20);
      write(1, line, n);
      n = read(fd[0], line, 20);
      write(1, line, n);
    } else { // parent
      close(fd[0]);
      sleep(1);
      write(fd[1], "Hello world\n", 12);
      sleep(1);
      write(fd[1], "Hello world\n", 12);
    }
    return 0;
}
