#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int main (int argc, char **argv) {
  if (lseek(STDIN_FILENO, 0, SEEK_CUR) != -1) {
    printf("Can be lseek\n");
  } else {
    printf("Can not be lseek\n");
  }
  return 0;
}
