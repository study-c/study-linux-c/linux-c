  Each running program, called a process, has associated with it a number of file descriptors. These are small
  integers that you can use to access open files or devices. How many of these are available will vary depending
  on how the UNIX system has been configured. When a program starts, it usually has three of these descriptors
  already opened
  0 : Standard input
  1 : Standard output
  2 : Standard error

