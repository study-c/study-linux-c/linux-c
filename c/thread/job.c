#include <stdio.h>
#include <stdlib.h>
#include "job.h"

struct job_str {
  fn fn;
  void *arg;
  job next;
};

job job_init(fn f, void *arg) {
  job j = (job)malloc(sizeof(struct job_str));
  j->fn = f;
  j->arg = arg;
  return j;
};

void destroy_job(job j) {
  free(j);
};

void run_job(job j) {
  j->fn(j->arg);
  destroy_job(j);
};

job getNextJob(job j) {
  return j->next;
};

void setNextJob(job j, job n) {
  j->next = n;
}
