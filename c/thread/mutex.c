#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#define NUM_THREADS 5

int count = 0;
pthread_mutex_t mutexCount = PTHREAD_MUTEX_INITIALIZER;
void *addCount1() {
    pthread_mutex_lock(&mutexCount);
    for(int i=0; i< 5; i++) {
        count++;
        printf("Count: %d\n", count);
    }
    pthread_mutex_unlock(&mutexCount);
}

void *addCount2() {
    pthread_mutex_lock(&mutexCount);
    for(int i=0; i< 10; i++) {
        count++;
        printf("Count: %d\n", count);
    }
    pthread_mutex_unlock(&mutexCount);
}

int main(int argc, char **argv) {
     pthread_t threads[NUM_THREADS];
     pthread_t threads1[NUM_THREADS];
     int rc, rc1;
     long t;
     for(t=0; t < NUM_THREADS; t++) {
         printf("In main: creating thread %ld\n", t);
         rc = pthread_create(&threads[t], NULL, addCount1, NULL);
         if (rc) {
             printf("ERROR creating thread %d", rc);
             exit(1);
         }
     }

     for(t=0; t < NUM_THREADS; t++) {
         printf("In main: creating thread2 %ld\n", t);
         rc1 = pthread_create(&threads1[t], NULL, addCount2, NULL);
         if (rc1) {
             printf("ERROR creating thread %d", rc1);
             exit(1);
         }
     }

     for(t=0; t < NUM_THREADS; t++) {
          pthread_join(threads[t], NULL);
          pthread_join(threads1[t], NULL);
     }
     return 0;
}
