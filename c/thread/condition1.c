#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#define NUM_THREADS 5

typedef struct node_metux {
  int count;
  pthread_mutex_t lock;
  pthread_cond_t  cond;
  int id;
  struct node_metux *next;
} *node;

node node_int(int id) {
  node first;
  if ((first = (node)malloc(sizeof(struct node_metux))) != NULL) {
    first->count = 1;
    first->id = id;
    if (pthread_mutex_init(&first->lock, NULL) != 0) {
      free(first);
      return NULL;
    }
    if (pthread_cond_init(&first->cond, NULL) != 0) {
      free(first);
      return NULL;
    }
  }
  return first;
}

void *node_add_count(void *arg) {
  node list = (struct node_metux *)arg;
  for (int i = 0; i < 5; i++) {
    /* code */
    pthread_mutex_lock(&list->lock);
    list->count += 1;
    pthread_cond_signal(&list->cond);
    printf("%d\n", list->count);
    sleep(1);
    pthread_mutex_unlock(&list->lock);
  }
  return list;
}

void *node_eat_count(void *arg) {
  node list = (struct node_metux *)arg;
  pthread_mutex_lock(&list->lock);
  while(list->count < 3) {
    printf("start wait\n");
    pthread_cond_wait(&list->cond, &list->lock);
  }
  list->count -= 2;
  printf("%d\n", list->count);
  pthread_mutex_unlock(&list->lock);
  return list;
}

int main(int argc, char **argv) {
  node list = node_int(1);
  pthread_t add_threads[3];
  pthread_t eat_threads[2];
  int rc, rc1;
  for(int t=0; t < 2; t++) {
      printf("In main: creating eat thread %d\n", t);
      rc1 = pthread_create(&eat_threads[t], NULL, node_eat_count, (void *)list);
      if (rc1) {
          printf("ERROR creating eat thread %d", rc1);
          exit(1);
      }
  }

  for(int t=0; t < 3; t++) {
      printf("In main: creating add thread %d\n", t);
      rc = pthread_create(&add_threads[t], NULL, node_add_count, (void *)list);
      if (rc) {
          printf("ERROR creating add thread %d", rc);
          exit(1);
      }
  }

  for(int t=0; t < 3; t++) {
       pthread_join(add_threads[t], NULL);
  }
  for(int t=0; t < 2; t++) {
       pthread_join(eat_threads[t], NULL);
  }
  printf("list count is %d\n",list->count );
  return 0;
}
