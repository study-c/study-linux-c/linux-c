#ifndef J_QUEUE_H
#define J_QUEUE_H
#define MAX_QUEUE_SIZE 2000

struct j_queue_str;
typedef struct j_queue_str *j_queue;

j_queue q_init();

void q_put(j_queue q, void *fn, void *arg);
void *q_get(j_queue q);
void destroy_q(j_queue q);
void print_q(j_queue q);

#endif
