#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#define NUM_THREADS 5

typedef struct node_metux {
  int count;
  pthread_mutex_t lock;
  int id;
  struct node_metux *next;
} *node;

node node_int(int id) {
  node first;
  if ((first = malloc(sizeof(struct node_metux))) != NULL) {
    first->count = 1;
    first->id = id;
    if (pthread_mutex_init(&first->lock, NULL) != 0) {
      free(first);
      return NULL;
    }
  }
  return first;
}

void *node_update(void* arg) {
  node list = (struct node_metux *)arg;
  pthread_mutex_lock(&list->lock);
  for (int i = 0; i < 5; i++) {
    /* code */
    list->count += 1;
    printf("%d\n", list->count);
  }

  pthread_mutex_unlock(&list->lock);
  return list;
}

int main(int argc, char **argv) {
  node list = node_int(1);
  printf("list count is %d\n",list->count );
  pthread_t threads[NUM_THREADS];
  int rc;
  long t;
  for(t=0; t < NUM_THREADS; t++) {
      printf("In main: creating thread %ld\n", t);
      rc = pthread_create(&threads[t], NULL, node_update, (void *)list);
      if (rc) {
          printf("ERROR creating thread %d", rc);
          exit(1);
      }
  }
  for(t=0; t < NUM_THREADS; t++) {
       pthread_join(threads[t], NULL);
  }
  printf("list count is %d\n",list->count );
  return 0;
}
