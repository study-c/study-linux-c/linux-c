#ifndef JOB_H
#define JOB_H
#ifdef __cplusplus
extern "C" {
#endif

typedef void (*fn)(void *arg);
struct job_str;
typedef struct job_str *job;

job job_init(fn f, void *arg);

void run_job(job b);

void destroy_job(job b);

job getNextJob(job j);

void setNextJob(job j, job n);

#ifdef __cplusplus
}
#endif
#endif
