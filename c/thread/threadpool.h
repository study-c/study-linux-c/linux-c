// threadpool head

#ifdef THREADPOOL_H
#define THREADPOOL_H

// max pool size
#define MAX_POOL_NUM 10

//simple declare universal struct
struct thpool_s;
typedef thpool_s *threadpool;

//simple declare a point ot function return void
typedef void (*pool_fn)(void *);

//threadpool create
threadpool threadpool_init(int thread_size);

//add function to pool
int pool_add_fn(threadpool pool, pool_fn fn, void *arg);

int pool_destroy(threadpool pool);

#endif
