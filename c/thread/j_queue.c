#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include "j_queue.h"
#include "job.h"


struct j_queue_str {
  job front;
  job rear;
  pthread_mutex_t lock;
  pthread_cond_t  cond_not_full;
  pthread_cond_t  cond_not_empty;
  int size;
};

j_queue q_init() {
  j_queue q;
  if ((q = (j_queue)malloc(sizeof(struct j_queue_str))) != NULL) {
    q->size = 0;
    q->front = NULL;
    q->rear = NULL;
    if (pthread_mutex_init(&q->lock, NULL) != 0) {
      free(q);
      return NULL;
    }
    if (pthread_cond_init(&q->cond_not_full, NULL) != 0) {
      free(q);
      return NULL;
    }
    if (pthread_cond_init(&q->cond_not_empty, NULL) != 0) {
      free(q);
      return NULL;
    }
  }
  return q;
};

void q_put(j_queue q, void *func, void *arg) {
  pthread_mutex_lock(&q->lock);
  while(q->size == MAX_QUEUE_SIZE) {
    pthread_cond_wait(&q->cond_not_full, &q->lock);
  }
  fn f = (fn)func;
  job j = job_init(f, arg);
  switch(q->size) {
    case 0:
      q->size++;
      q->front = j;
      q->rear = j;
      break;
    default:
      q->size++;
      setNextJob(q->rear, j);
      q->rear = j;
      break;
  }
  pthread_cond_signal(&q->cond_not_empty);
  pthread_mutex_unlock(&q->lock);
};

void *q_get(j_queue q) {
  pthread_mutex_lock(&q->lock);
  while(q->size == 0) {
    pthread_cond_wait(&q->cond_not_empty, &q->lock);
  }
  job re = q->front;
  switch(q->size) {
    case 1:
      q->size = 0;
      q->front = NULL;
      q->rear = NULL;
      break;
    default:
      q->size--;
      q->front = getNextJob(q->front);
      break;
  }
  pthread_cond_signal(&q->cond_not_full);
  pthread_mutex_unlock(&q->lock);
  setNextJob(re, NULL);
  return re;
};

void destroy_q(j_queue q) {
  while(getNextJob(q->front)) {
    destroy_job(q->front);
    q->front = getNextJob(q->front);
  }
  free(q);
};
