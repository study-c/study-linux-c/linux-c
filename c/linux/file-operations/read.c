#include <unistd.h>
#include <stdlib.h>
int main()
{
    char buff[128];
    int nread;
    nread = read(0,buff,128);

    if(nread == -1)
        write(2, "A read error has occurred\n", 26);
    if(write(1,buff,nread) !=nread)
        write(2,"write error has occurred\n",25);
    return 0;
}
