#include <stdio.h>

int sum(int a,int b)
{
    return a+b;
}

int main(int argc, char *argv[])
{
    int result;
    int (*mysum)(int,int) = &sum;
    result = mysum(6,8);
    printf("sum is %d\n",result);
}

