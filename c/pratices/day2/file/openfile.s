	.file	"openfile.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"text"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"Failed to open or create the file"
	.section	.rodata.str1.1
.LC2:
	.string	"Hello World\n"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB3:
	.section	.text.startup,"ax",@progbits
.LHOTB3:
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB57:
	.cfi_startproc
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	xorl	%eax, %eax
	movl	$384, %edx
	movl	$1090, %esi
	movl	$.LC0, %edi
	call	open
	cmpl	$-1, %eax
	je	.L5
	movl	%eax, %ebx
	movl	$15, %edx
	movl	$.LC2, %esi
	movl	%eax, %edi
	call	write
	movl	%ebx, %edi
	call	close
	xorl	%eax, %eax
	popq	%rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L5:
	.cfi_restore_state
	movl	$.LC1, %edi
	call	puts
	movl	$1, %edi
	call	exit
	.cfi_endproc
.LFE57:
	.size	main, .-main
	.section	.text.unlikely
.LCOLDE3:
	.section	.text.startup
.LHOTE3:
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
