	.file	"openfile.c"
	.section	.rodata
.LC0:
	.string	"text"
	.align 8
.LC1:
	.string	"Failed to open or create the file"
.LC2:
	.string	"Hello World\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movq	%rsi, -32(%rbp)
	movl	$384, %edx
	movl	$1090, %esi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	open
	movl	%eax, -4(%rbp)
	cmpl	$-1, -4(%rbp)
	jne	.L2
	movl	$.LC1, %edi
	call	puts
	movl	$1, %edi
	call	exit
.L2:
	movl	-4(%rbp), %eax
	movl	$15, %edx
	movl	$.LC2, %esi
	movl	%eax, %edi
	call	write
	movl	-4(%rbp), %eax
	movl	%eax, %edi
	call	close
	movl	$0, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
