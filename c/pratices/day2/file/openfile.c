#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    int fd;
    //char buf[100];
    fd = open("text",O_CREAT | O_RDWR | O_APPEND,0600);
    if(fd == -1)
    {
        printf("Failed to open or create the file\n");
        exit(1);
    }

    write(fd,"Hello World\n",15);
    close(fd);
    return 0;
}

