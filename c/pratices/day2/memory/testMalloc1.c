#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct{
    char name[20];
    int age;
    int height;
} person;

int main(int argc, char *argv[])
{
    person *jerry = (person *)malloc(sizeof(person));
    strcpy(jerry->name,"Jerry Chen");
    jerry->age = 31;
    jerry->height = 171;
    printf("name : %s\n age : %d\n height : %d\n",jerry->name,jerry->age,jerry->height);
    free(jerry);
    return 0;   
}
