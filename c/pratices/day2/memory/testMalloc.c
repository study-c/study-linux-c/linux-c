#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
    char *ptr;
    ptr = (char *)malloc(20);

    if(ptr==NULL)
    {
        printf("Failed to locate the memory!\n");
        exit(1);
    }

    strcpy(ptr,"Hello World!");
    printf("prt:%s\n",ptr);

    free(ptr);

    return 0;
}
