#include <stdio.h>
#include <unistd.h>

int main(int argc,char *argv[])
{
    int childpid;
    int c1 =0, c2=0;
    printf("Before fork\n");

    childpid = fork();

    if(childpid ==0)
    {
        printf("This is a child process\n");
        while(c1 <10){
            printf("Child process:%d\n",c1);
            sleep(1);
            c1++;
        }
    }else{
    
        printf("This is a parent process\n");
        while(c2 <10){
            printf("Parent process:%d\n",c2);
            sleep(1);
            c2++;
        }
    
    }
    return 0;
}
