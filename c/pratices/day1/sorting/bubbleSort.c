#include <stdio.h>
void bubbleSort(int len, int *arr);
void swap(int *a,int *b);
int main(void)
{
    int arr[10] = {22,3,6,1,6,4,0,33,11,8};
    bubbleSort(10, arr);
}

void bubbleSort(int len, int arr[])
{
    for(int i=0;i<len;i++)
    {
        for(int j = 1; j<len -i;j++)
        {
            if(arr[j-1] > arr[j])
            {
                swap(&arr[j-1],&arr[j]);
            }
        }
    }
}
void swap(int *a,int *b)
{
    int temp;
    temp=*a;
    *a = *b;
    *b = temp;
}
