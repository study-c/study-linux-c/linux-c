#include <stdio.h>
void swap(int *a, int *b);
int main(void)
{
    int a=3,b=4;
    printf("%d %d\n",a,b);
    swap(&a,&b);
    printf("%d %d\n",a,b);
}



void swap(int *a, int *b)
{
    int temp;
    temp = *a;
    *a=*b;
    *b=temp;
}
