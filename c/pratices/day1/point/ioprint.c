#include <stdio.h>
#include <string.h>

#define MAXLINES 5000
char *lineptr[MAXLINES];

int readlines(char *lineptr[],int nlines);
void writelines(char *lineptr[], int nlines);

void qsort(char *lineptr[], int left, int right);

int main(void)
{
    int nlines;
    if ((nlines=readlines(lineptr,MAXLINES)) >=0){
        qsort(lineptr,0,nlines-1);
        writelines(lineptr,nlines);
        return 0;
    }else{
        printf("error:input too many\n");
        return 1;
    }
}
#define MAXLEN 1000
int getline(char *,int);
char *alloc(int);
int readlines(char *lineptr[], int maxlines){
    int len,nlines;
    char *p,line[MAXLEN];
    nlines = 0;
    while((len = getline(line,MAXLEN)>0))
            if(nlines>=maxlines ||(p=alloc(len))==NULL)
            return -1;
            else {
            line[len-1]='\0';
            strcpy(p,line);
            lineptr[nlines++]=p;
            }
    return nlines;
 }

            void writelines(char *lineptr[], int nlines){
            int i;
            for(i=0;i<nlines;i++)
            printf("%d\n", lineptr[i]);
            }
            void qsort(char *lineptr[], int left,int right)
            {
            int i,last;
            void swap(char *lineptr[],int i, int j);
            if(left>right)
                return;
            swap(lineptr,left,(left+right)/2);
            last = left;
            for(i=left+1;i<=right;i++)
                if(strcmp(lineptr[i],lineptr[left])<0)
                    swap(lineptr,++last,i);
            swap(lineptr,left,last);
            qsort(lineptr,left,last-1);
            qsort(lineptr,last+1,right);
            }
void swap(char *lineptr[],int i,int j)
{
    char *temp;
    temp = lineptr[i];
    lineptr[i] = lineptr[j];
    lineptr[j] = temp; 
}
