#include <stdio.h>
int main(void)
{
    int x=1,y=2;
    int *ip;
    ip = &x;
    printf("x:%d y:%d\n",x,y);
    y = *ip;
    printf("x:%d y:%d\n",x,y);
    *ip=0;
    printf("x:%d y:%d\n",x,y);
}
