#include <stdio.h>
#include <assert.h>
int main(void)
{
    int c;
    while((c = getchar()) != EOF)
    {
        assert(c != 'a');
        putchar(c);
    }
    return 0;
}
