#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>

#include <unistd.h>

int main(int argc, char *argv[])
{
    int fd;
    char buf[7];
    fd = open("test.txt",O_CREAT | O_WRONLY,0600);
    if(fd == -1)
    {
        printf("failed to create a file");
        exit(1);
    }

    write(fd,"hello\n",6);
    close(fd);
    fd = open("test.txt",O_RDONLY);
    if(fd == -1)
    {
        printf("Failed to open a file");
        exit(1);
    }
    read(fd,buf,6);
    buf[6]='\0';
    close(fd);
    printf("buf: %s\n",buf);
    return 0;
}
