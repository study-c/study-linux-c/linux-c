#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>

int main(int argc, char *argv[])
{
    DIR *dir;
    struct dirent *sd;

    char  *path;

    path = ".";
    if(argv[1]) path = argv[1];

    dir = opendir(path);
    if(dir == NULL)
    {
        printf("Error: Unable to open directory");
        exit(1);
    }
    while((sd=readdir(dir)) != NULL)
    {
        printf(">> %s \n",sd->d_name);
    }
    closedir(dir);
    return 0;

}
