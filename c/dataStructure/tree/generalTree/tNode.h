/*
 *	Tree Node ADT
 *  Specification for the general Tree Node ADT
 *	author Jerry Chen
 *	Date: Jul 2013
*/


#include <stdbool.h>

struct tNode_int;
typedef struct tNode_int *tNode;

void init_TNode(tNode *tp,void *o,int l);
void setTNData(tNode t,void *o);
void *getTNData(tNode t);
void setTNLevel(tNode t,int l);
int getTNLevel(tNode t);
void setTNChild(tNode t,tNode n);
tNode getTNChild(tNode t);
void setTNSibling(tNode t,tNode n);
tNode getTNSibling(tNode t);
