/*
 * Node
 * Specification for the Node ADT
 * Author:Jerry Chen
 * Date: Jul 2013
 *
*/

#ifndef NODE_H
#define NODE_H

#include <stdbool.h>

struct node_int;
typedef struct node_int *node;

node init_node(void *o);
void setDataN(node v, void *o);
void setNextN(node v, node n);
void *getDataN(node v);
node getNextN(node v);

#endif
