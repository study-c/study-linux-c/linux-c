/*
 * List
 * Specification for the List ADT
 * Author Jerry Chen
 * Date Jul 2013
 *
*/
#ifndef LIST_H
#define LIST_H

#include <stdbool.h>
struct list_int;
typedef struct list_int *list;

list init_list();
bool isEmptyL(list l);
void insert(list l, void *o);
void print(list l);
void insert_first(list l);
void insert_last(list l);
void* find(void *o);

#endif
