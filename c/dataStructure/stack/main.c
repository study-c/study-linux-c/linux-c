#include <stdio.h>
#include <stdlib.h>
#include "node.h"
#include "stack.h"

int main(int argc, char *agrv[])
{
	node t,t1,t2;
  	int a = 5;
    	char *s = "hello";
  	char *s1 = "good";
  	char *s2 = "day";
  	init_node(&t,&a);
  	init_node(&t1,s);
	printf("*********************test of node  ****************************\n");
  	printf("%d \n",*(int *)getDataN(t)); //5
    	setDataN(t1,s1);
    	printf("%s \n",getDataN(t1)); // good
    	setNextN(t,t1);
    	printf("Node t1 address:%p\nt->next address %p\n",t1,getNextN(t));//address of node t1 and t2
	printf("*********************test of stack ****************************\n");

    	stack tests;
	init_stack(&tests);

  	printf("Stack: tests address:%p\n",tests);// address of stack

  	printf("After initialize test IsEmpty:%s \n", isEmptyS(tests) ? "True" :"False"); //true

  	push(tests,&a);

	printf("After push a test IsEmpty:%s \n", isEmptyS(tests) ? "True" :"False");//false
   	printf("*********************push method test ****************************\n");
	push(tests,t1);

  	push(tests,s);
  	push(tests,s1);

  	printf("%s \n",toStringS(tests,"a"));// a a a a
  	printf("*********************top method test ****************************\n");
  	printf("%s \n",(char *)top(tests)); // good
  	printf("%s \n",toStringS(tests,"a")); // a a a a
    	printf("*********************pop method test ****************************\n");
	pop(tests);
  	printf("%s \n",(char *)top(tests)); //hello
  	pop(tests);
	pop(tests);
	pop(tests);
	printf("%s \n",toStringS(tests,"a"));// <>
  	return 0;

}
