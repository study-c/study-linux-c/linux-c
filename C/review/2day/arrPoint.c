#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
    char arr[] = "abcd";
    char *p = arr;
    for(int i = 0; i < strlen(arr); i++)
    {
        printf("%c ", arr[i]);
    }
    for(int i = 0; i < strlen(arr); i++)
    {
        printf("%c ", *(p+i));
    }
    {
        // arr is not assignable why ? -> memory type
        // arr = arr + 1;
    }

}