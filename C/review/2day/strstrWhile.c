#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int strInStrOccTimes(char *str/*in*/, char *target/*in*/);
int strTrim(char *in, char *out);
int strReverse(char *in, char *out);
void strRecursiveReverse(char *in, char *out);
int main(int argc, char *argv[])
{
    char *str = "   abcd123abcd123abcdefgabcdllkj  ";
    char *target = "ab";
    char out[100];
    int count = strInStrOccTimes(str, target);
    strTrim(str, out);
    printf("count:%s \n", out);
    strReverse(str, out);
    printf("count:%s \n", out);
    strRecursiveReverse(str, out);
    printf("count:%s \n", out);
}

void strRecursiveReverse(char *in, char *out)
{
    if (in == NULL)
    {
        return;
    }
    if (*in == '\0')
    {
        return;
    }
    strRecursiveReverse(in+1, out+1);
    *out = *in;
}

int strReverse(char *in, char *out)
{
    strcpy(out, in);
    char *end = out + strlen(out)  - 1;
    char tem;
    if(in == NULL)
    {
        return -1;
    }

    while(out < end)
    {
        tem = *out;
        *out = *end;
        *end = tem;
        ++out;
        --end;
    }
    return 0;
}

int strTrim(char *in, char *out)
{
    char *start = in;
    char *end;
    if (in == NULL)
    {
        return -1;
    }
    while(isspace((char)*start)) 
    {
        start++;
    }
    if(*start == 0)
    {
        return -2;
    }

    end = start + strlen(start) - 1;


    while(end > start && isspace((char)*end))
    {
        end--;
    }
    strncpy(out, start, end - start);    
    return 0;
}

int strInStrOccTimes(char *str, char *target)
{
    int count = 0;
    char *p = str;
    if (str == NULL || target == NULL)
    {
        printf("str is NULL or target is NULL \n");
        return -1;
    }
    while(( p = strstr(p, target) ))
    {
        if (p != NULL)
        {
            count++;
            p = p + strlen(target);
        } else {
            break;
        }
    }
    return count;
}