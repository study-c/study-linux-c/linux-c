#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int sort(char **in1/*in*/, int len1, char buff[][30]/*in*/, int len2, char ***out/*out*/, int *outLen);
void freeSort(char **p, int len);
void freeSortComplete(char ***p, int len);
int main(int argc, char *argv[])
{
    char *in1[] = {"bb", "cc", "dd", "ff", "gg"};
    char buff[10][30] = {"asfd", "fasfd", "tasf", "afasf"};
    char **out = NULL;
    int len1, len2 = 3, len3;
    len1 = sizeof(in1) / sizeof(char *);
    int re = sort(in1, len1, buff, len2, &out, &len3);
    if (re != 0)
    {
        printf("sort function error %d \n", re);
        return re;
    }

    for(int i = 0; i < len3; i ++)
    {
        printf("%s \n", out[i]);
    }

    return 0;
}

int sort(char **in1/*in*/, int len1, char buff[][30]/*in*/, int len2, char ***out/*out*/, int *outLen)
{
    int lenTotal = len1 + len2, i = 0, j = 0, tmpLen;
    char **tmp = NULL;
    char *swapTem = NULL;
    tmp = (char **)malloc(lenTotal * sizeof(char *));
    if (tmp == NULL)
    {
        return -1;
    }
    for (i = 0; i < len1; i++)
    {
        tmpLen = strlen(in1[i]) + 1;
        tmp[i] = (char *)malloc(tmpLen * sizeof(char));
        strcpy(tmp[i], in1[i]);
    }
    for(j = 0; j < len2; j++, i++)
    {
        tmpLen = strlen(buff[j]) + 1;
        tmp[i] = (char *)malloc(tmpLen * sizeof(char));
        strcpy(tmp[i], buff[j]);
    }
    for(i = 0; i < lenTotal; i++)
    {
        for(j = i+1; j < lenTotal; j++)
        {
            if (strcmp(tmp[i], tmp[j]) > 0)
            {
                swapTem = tmp[i];
                tmp[i] = tmp[j];
                tmp[j] = swapTem;
            }
            
        }
    }
    *out = tmp;
    *outLen = lenTotal;
    return 0;
}
void freeSort(char **p, int len)
{
    if (p == NULL)
    {
        return;
    }
    for (int i = 0; i < len; i++)
    {
        free(p[i]);
    }
    free(p);
}

void freeSortComplete(char ***p, int len)
{
    if (p == NULL)
    {
        return;
    }
    char **tmp = *p;
    if (tmp == NULL)
    {
        return;
    }
    for (int i = 0; i < len; i++)
    {
        free(tmp[i]);
    }
    free(tmp);
    *p = NULL;
}