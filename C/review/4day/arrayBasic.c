// array type
// complex data type
// array type = elment type + size eg int arr[5] type is int [5]
// alias of certain size memory size
// typedef int (MYINT)[5]
// MYINT arr

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef int (MYINT5)[5];

int main(int argc, char *argv[])
{
    MYINT5 arr = {1};
    for (int i = 0; i < 5; i++)
    {
        printf("%d \n", arr[i]);
    }
    for (int i = 0; i < 5; i++)
    {
        arr[i] = i + 4;
    }
    for (int i = 0; i < 5; i++)
    {
        printf("%d \n", arr[i]);
    }

    printf("%p \n", arr + 1);

    // array of points
    int *ap[5];
    // point to array
    int (*pa)[5];
    MYINT5 *pa2;
    pa2 = &arr;
    for (int i = 0; i < 5; i++)
    {
        // turn 2 level point to one level point
        printf("%d \n",(*pa2)[i]);
    }
    // 2 level point to array type
    typedef int (*Parr)[5];
    Parr pa3;    
    pa3 = &arr;
    for (int i = 0; i < 5; i++)
    {
        // turn 2 level point to one level point
        printf("%d \n",(*pa2)[i]);
    }

    return 0;
}
