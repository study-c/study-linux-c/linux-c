#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Person 
{
    int age;
    char name[50];
} Person; // define one

struct _Person 
{
    int age;
    char name[50];
} p1, p2 = {10, "aa"}; // define two

struct 
{
    int age;
    char name[50];
} p3, p4; // define three anonymous

void printPerson( Person *p, int len);

int main(int argc, char *argv[])
{
    Person p0;
    printf("name: %s, age: %d \n", p2.name, p2.age);
    p0.age = 99;
    strcpy(p0.name, "jerry");
    printf("name: %s, age: %d \n", p0.name, p0.age);
    {
        // point 
        Person *pp = NULL;
        pp = &p0;
        pp->age = 18;
        printf("name: %s, age: %d \n", p0.name, p0.age);
    }

    {
        Person p5;
        // value pass
        p5 = p0;
        printf("name: %s, age: %d \n", p5.name, p5.age);
        p5.age = 100;
        strcpy(p5.name, "tom");
        printf("name: %s, age: %d \n", p5.name, p5.age);
        printf("name: %s, age: %d \n", p0.name, p0.age);
    }

    {
        Person ap[5];
        for (int i = 0; i < 5; i++)
        {
            printf("please enter age");
            scanf("%d", &(ap[i].age));
        }
        printPerson(ap, 5);
    }
    return 0;
}

void printPerson(Person *p, int len)
{
    for(int i = 0; i < len; i++)
    {
        printf("%d\t", p[i].age);
    }
}

