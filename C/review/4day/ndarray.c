#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
    int arr[3][5], tmp = 0;
    for (int i = 0; i <3; i++)
    {
        for(int j = 0; j < 5; j ++) 
        {
            arr[i][j] = tmp++;
        }
    }

    printf("arr: %p, arr + 1: %p \n", arr, arr + 1);
    printf("&arr: %p, &arr + 1: %p \n", &arr, &arr + 1);

    {
        typedef int (*pArr)[5];
        pArr pa1 = arr;
        for (int i = 0; i < 3; i++)
        {
            for(int j = 0; j < 5; j ++) 
            {
                printf("%d \t", pa1[i][j]);
            }
        }        
    }
    

    return 0;
}