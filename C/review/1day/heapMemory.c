#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct person 
{
    int age;
    char *name;
} ps;
ps getStructMemory()
{
    ps p1;
    p1.age = 3;
    p1.name = "jerry";
    return p1;
}
char * getHeapMemory1(int num)
{
    char *p1;
    p1 = (char *)malloc(sizeof(char) * num);
    if (p1 == NULL)
    {
        return NULL;
    }
    printf("%p \n", p1);
    return p1;
}

int main()
{
    char *p1 = NULL;
    p1 = getHeapMemory1(100);
    ps aa = getStructMemory();
    printf("aa: %d \n", aa.age);
    printf("sizeof p1: %p\n", p1);
    if (p1 == NULL) {
        return 1;
    }
    strcpy(p1, "adcd");
    printf("p1: %s \n", p1);
}
