#include <stdio.h>

int main()
{
    printf("Hello\tWorld\n");
    printf("Hello\aWorld\b\n");
    printf("Hello\123World\x33\n");
    char a, b;
    a = 120;
    b = 121;
    printf("a ascii: %c, b ascii: %c \n", a, b);
    printf("a int: %d, b int: %d \n", a, b);
    a = '#';
    b = '&';
    printf("a ascii: %c, b ascii: %c \n", a, b);
    printf("a int: %d, b int: %d \n", a, b);

}
