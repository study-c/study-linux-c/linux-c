#include <stdio.h>

int main(int argc, char **argv)
{
    printf("float length: %lu \n", sizeof(float));
    printf("double length: %lu \n", sizeof(double));
    printf("long double length: %lu \n", sizeof(long double));
    return 0;
}
