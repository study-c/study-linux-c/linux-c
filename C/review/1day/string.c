#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// string end with \0
// no string type in c but char *
// could be in stack, heap or global static 

int main(int argc, char *argv[])
{
    char str[] = "aadb";
    int len = strlen(str);
    unsigned size = sizeof(str);

    printf("len: %d, size: %u \n", len, size);
}