#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// direct pass
void printArrPoints(char **arr, int length);
void sort2LevelPoint(char **arr, int length);
int initHeap2LevelPoint(char ***arr, int length);
void freeHeap2LevelPoint(char ***arr, int length);
int main(int argc, char *argv[])
{
    // array of points
    char **arr = NULL;
    int length = 5;
    initHeap2LevelPoint(&arr, length);
    printArrPoints(arr, length);

    sort2LevelPoint(arr, length);
    printf("After Sorting \n");
    printArrPoints(arr, length);
    freeHeap2LevelPoint(&arr, length);
    return 0;
}

void freeHeap2LevelPoint(char ***arr, int length)
{
    char **tmp = *arr;
    for (int i = 0; i < length; i++)
    {
        free(*(tmp + i));
    }

    free(tmp);

    *arr = NULL;    
}

int initHeap2LevelPoint(char ***arr, int length)
{
    char **p = NULL;
    if (arr == NULL)
    {
        return -1;
    }
    *arr = (char **)malloc(sizeof(char *) * length);
    for (int i = 0; i < length; i ++)
    {
        *(*arr + i) = (char *)malloc(sizeof(char) * 30);
        sprintf(*(*arr + i), "%d%d%d", i, i+1, i);
    }
    // *arr = p; 
    return 0;
}
void printArrPoints(char **arr, int length)
{
    for (int i = 0; i < length; i ++)
    {
        printf("%s \n", *(arr + i));
    } 
}

void sort2LevelPoint(char **arr, int length)
{
    char *tem;
    for (int i = 0; i < length; i++)
    {
        for (int j = i; j < length; j++)
        {
            if (strcmp(arr[i], arr[j]) < 0)
            {
                tem = arr[i];
                arr[i] = arr[j];
                arr[j] = tem; 
            }
        }
    }    
}