#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int strsplit(char *in, char ***out, char c, int *num);

int main(int argc, char *argv[])
{
    char *in = "abc,aa,dd,ccadf,pp,99,uu,";
    char c = ',';
    char **out;
    int num = 0;

    strsplit(in, &out, c, &num);

    for (int i = 0; i < num; i++)
    {
        printf("%s \n", out[i]);
    }

    printf("%d", num);

}

int strsplit(char *in, char ***out, char c, int *num)
{
    char *p = NULL, *pTem = NULL;
    int count = 0;

    if (in == NULL)
    {
        printf("Input string can not be NULL");
        return -1;
    }
    p = in;
    pTem = in;
    do 
    {
        p = strchr(p, c);
        if (p != NULL)
        {
            if (p - pTem > 0)
            {
                count ++;
                pTem = p = p + 1;
            }
        }
        else
        {
            break;
        } 
    } while (*p != '\0');

    *num = count;

    char **tem = NULL;

    tem = (char **)malloc(sizeof(char *) * count);

    p = in;
    pTem = in;
    count = 0;
    do 
    {
        p = strchr(p, c);
        if (p != NULL)
        {
            if (p - pTem > 0)
            {
                tem[count] = (char *)malloc(sizeof(char) * (p - pTem));
                strncpy(tem[count], pTem, p-pTem);
                tem[count][p-pTem] = '\0';
                count ++;
                pTem = p = p + 1;
            }
        }
        else
        {
            break;
        } 
    } while (*p != '\0');

    *out = tem;

    return 0;
}