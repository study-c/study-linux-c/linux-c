	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 13
	.globl	_main
	.p2align	4, 0x90
_main:                                  ## @main
	.cfi_startproc
## BB#0:
	pushq	%rbp
Lcfi0:
	.cfi_def_cfa_offset 16
Lcfi1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi2:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r12
	pushq	%rbx
	subq	$160, %rsp
Lcfi3:
	.cfi_offset %rbx, -48
Lcfi4:
	.cfi_offset %r12, -40
Lcfi5:
	.cfi_offset %r14, -32
Lcfi6:
	.cfi_offset %r15, -24
	leaq	-96(%rbp), %rax
	leaq	L_.str.5(%rip), %rcx
	leaq	L_.str.4(%rip), %rdx
	leaq	L_.str.3(%rip), %r8
	leaq	L_.str.2(%rip), %r9
	leaq	L_.str.1(%rip), %r10
	leaq	L_.str(%rip), %r11
	xorl	%ebx, %ebx
	movl	$48, %r14d
	movl	%r14d, %r15d
	movq	___stack_chk_guard@GOTPCREL(%rip), %r12
	movq	(%r12), %r12
	movq	%r12, -40(%rbp)
	movl	$0, -100(%rbp)
	movl	%edi, -104(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rax, %rsi
	movq	%rsi, %rdi
	movl	%ebx, %esi
	movq	%rdx, -128(%rbp)        ## 8-byte Spill
	movq	%r15, %rdx
	movq	%r10, -136(%rbp)        ## 8-byte Spill
	movq	%r11, -144(%rbp)        ## 8-byte Spill
	movq	%rax, -152(%rbp)        ## 8-byte Spill
	movq	%rcx, -160(%rbp)        ## 8-byte Spill
	movq	%r9, -168(%rbp)         ## 8-byte Spill
	movq	%r8, -176(%rbp)         ## 8-byte Spill
	callq	_memset
	movq	-144(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -96(%rbp)
	movq	-136(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -88(%rbp)
	movq	-168(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -80(%rbp)
	movq	-176(%rbp), %rdi        ## 8-byte Reload
	movq	%rdi, -72(%rbp)
	movq	-128(%rbp), %r8         ## 8-byte Reload
	movq	%r8, -64(%rbp)
	movq	-160(%rbp), %r9         ## 8-byte Reload
	movq	%r9, -56(%rbp)
	movl	$6, -116(%rbp)
	movl	-116(%rbp), %esi
	movq	-152(%rbp), %rdi        ## 8-byte Reload
	callq	_printArrPoints
	leaq	-96(%rbp), %rdi
	movl	-116(%rbp), %esi
	callq	_sort2LevelPoint
	leaq	L_.str.6(%rip), %rdi
	movb	$0, %al
	callq	_printf
	leaq	-96(%rbp), %rdi
	movl	-116(%rbp), %esi
	movl	%eax, -180(%rbp)        ## 4-byte Spill
	callq	_printArrPoints
	movq	___stack_chk_guard@GOTPCREL(%rip), %rcx
	movq	(%rcx), %rcx
	movq	-40(%rbp), %rdx
	cmpq	%rdx, %rcx
	jne	LBB0_2
## BB#1:
	xorl	%eax, %eax
	addq	$160, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB0_2:
	callq	___stack_chk_fail
	.cfi_endproc

	.globl	_printArrPoints
	.p2align	4, 0x90
_printArrPoints:                        ## @printArrPoints
	.cfi_startproc
## BB#0:
	pushq	%rbp
Lcfi7:
	.cfi_def_cfa_offset 16
Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi9:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movl	%esi, -12(%rbp)
	movl	$0, -16(%rbp)
LBB1_1:                                 ## =>This Inner Loop Header: Depth=1
	movl	-16(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jge	LBB1_4
## BB#2:                                ##   in Loop: Header=BB1_1 Depth=1
	leaq	L_.str.7(%rip), %rdi
	movq	-8(%rbp), %rax
	movslq	-16(%rbp), %rcx
	movq	(%rax,%rcx,8), %rsi
	movb	$0, %al
	callq	_printf
	movl	%eax, -20(%rbp)         ## 4-byte Spill
## BB#3:                                ##   in Loop: Header=BB1_1 Depth=1
	movl	-16(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -16(%rbp)
	jmp	LBB1_1
LBB1_4:
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_sort2LevelPoint
	.p2align	4, 0x90
_sort2LevelPoint:                       ## @sort2LevelPoint
	.cfi_startproc
## BB#0:
	pushq	%rbp
Lcfi10:
	.cfi_def_cfa_offset 16
Lcfi11:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi12:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movl	%esi, -12(%rbp)
	movl	$0, -28(%rbp)
LBB2_1:                                 ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB2_3 Depth 2
	movl	-28(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jge	LBB2_10
## BB#2:                                ##   in Loop: Header=BB2_1 Depth=1
	movl	-28(%rbp), %eax
	movl	%eax, -32(%rbp)
LBB2_3:                                 ##   Parent Loop BB2_1 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	movl	-32(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jge	LBB2_8
## BB#4:                                ##   in Loop: Header=BB2_3 Depth=2
	movq	-8(%rbp), %rax
	movslq	-28(%rbp), %rcx
	movq	(%rax,%rcx,8), %rdi
	movq	-8(%rbp), %rax
	movslq	-32(%rbp), %rcx
	movq	(%rax,%rcx,8), %rsi
	callq	_strcmp
	cmpl	$0, %eax
	jle	LBB2_6
## BB#5:                                ##   in Loop: Header=BB2_3 Depth=2
	movq	-8(%rbp), %rax
	movslq	-28(%rbp), %rcx
	movq	(%rax,%rcx,8), %rax
	movq	%rax, -24(%rbp)
	movq	-8(%rbp), %rax
	movslq	-32(%rbp), %rcx
	movq	(%rax,%rcx,8), %rax
	movq	-8(%rbp), %rcx
	movslq	-28(%rbp), %rdx
	movq	%rax, (%rcx,%rdx,8)
	movq	-24(%rbp), %rax
	movq	-8(%rbp), %rcx
	movslq	-32(%rbp), %rdx
	movq	%rax, (%rcx,%rdx,8)
LBB2_6:                                 ##   in Loop: Header=BB2_3 Depth=2
	jmp	LBB2_7
LBB2_7:                                 ##   in Loop: Header=BB2_3 Depth=2
	movl	-32(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -32(%rbp)
	jmp	LBB2_3
LBB2_8:                                 ##   in Loop: Header=BB2_1 Depth=1
	jmp	LBB2_9
LBB2_9:                                 ##   in Loop: Header=BB2_1 Depth=1
	movl	-28(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -28(%rbp)
	jmp	LBB2_1
LBB2_10:
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	"wind"

L_.str.1:                               ## @.str.1
	.asciz	"peal"

L_.str.2:                               ## @.str.2
	.asciz	"go"

L_.str.3:                               ## @.str.3
	.asciz	"back"

L_.str.4:                               ## @.str.4
	.asciz	"love"

L_.str.5:                               ## @.str.5
	.asciz	"heart"

L_.str.6:                               ## @.str.6
	.asciz	"After Sorting \n"

L_.str.7:                               ## @.str.7
	.asciz	"%s \n"


.subsections_via_symbols
