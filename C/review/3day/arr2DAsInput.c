#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// direct pass
void printArrPoints(char arr[][30], int length);
void sort2LevelPoint(char arr[][30], int length);

// stem maters
int main(int argc, char *argv[])
{
    // array of points
    char arr[10][30] = {"wind", "peal", "go", "back", "love", "heart"};
    int length = sizeof(arr)/sizeof(arr[0]);
    printArrPoints(arr, 6);

    sort2LevelPoint(arr, 6);
    printf("After Sorting \n");
    printArrPoints(arr, 6);
    return 0;
}

void printArrPoints(char arr[][30], int length)
{
    for (int i = 0; i < length; i ++)
    {
        printf("%s \n", *(arr + i));
    } 
}

void sort2LevelPoint(char arr[][30], int length)
{
    char tem[30];
    for (int i = 0; i < length; i++)
    {
        for (int j = i; j < length; j++)
        {
            if (strcmp(arr[i], arr[j]) > 0)
            {
                strcpy(tem, arr[i]);
                strcpy(arr[i], arr[j]);
                strcpy(arr[j], tem);
            }
        }
    }    
}