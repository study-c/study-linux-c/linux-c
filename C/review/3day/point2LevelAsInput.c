#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// direct pass
void printArrPoints(char **arr, int length);
void sort2LevelPoint(char **arr, int length);

int main(int argc, char *argv[])
{
    // array of points
    char *arr[] = {"wind", "peal", "go", "back", "love", "heart"};
    int length = sizeof(arr)/sizeof(arr[0]);
    printArrPoints(arr, length);

    sort2LevelPoint(arr, length);
    printf("After Sorting \n");
    printArrPoints(arr, length);
    return 0;
}

void printArrPoints(char **arr, int length)
{
    for (int i = 0; i < length; i ++)
    {
        printf("%s \n", *(arr + i));
    } 
}

void sort2LevelPoint(char **arr, int length)
{
    char *tem;
    for (int i = 0; i < length; i++)
    {
        for (int j = i; j < length; j++)
        {
            if (strcmp(arr[i], arr[j]) > 0)
            {
                tem = arr[i];
                arr[i] = arr[j];
                arr[j] = tem; 
            }
        }
    }    
}