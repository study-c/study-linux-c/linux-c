#include <stdio.h>
#define MAXSIZE 100
int getLine(char s[]);
void copy(char from[], char to[]);

int main(){
    int len,max=0;
    char longest[MAXSIZE];
    char line[MAXSIZE];
    while((len=getLine(line)) > 0){ 
        if(len>max){
            max=len;    
            copy(line,longest);
        }
    }   
    if(max>0)    printf("%s",longest);
    return 0;
}

int getLine(char s[]){
    int c,i;
    for(i=0 ; (c=getchar()) != EOF && c!='\n';++i)
        s[i]=c;
    if(c=='\n'){
        s[i]=c;
        ++i;
    }
    s[i]='\0';
    return i;   
}

void copy(char from[], char to[]){
    int i=0;
    while((to[i]=from[i])!='\0'){
        ++i;
    }
}
