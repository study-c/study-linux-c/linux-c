#include <stdio.h>

struct person {
    char *name;
    int age;
};

int main (int argc, char *argv[]) {
    struct person p1;
    p1.name = "jerry";
    p1.age = 3;
    printf("The of p1 is %s \n", p1.name);
    printf("Age of p1 is %d \n", p1.age);
    int arr1[2][3] = {1,2,3,4,5,6};
    int *pa = arr1[0];
    printf("The arr1[1][1] is %d \n", arr1[1][1]);
    printf("The pa is point to %d \n", *(pa+1));
    printf("The arr1[1][1] is %d \n", *(*(arr1+1) + 1));
    return 0;
};

